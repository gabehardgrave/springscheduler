/**
 * Asynchronous Tasks.
 */
package scheduler;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This deliberately tries to create a race condition, to see if @Async locks shared data.
 * It does not. So, @Async tasks should really be "self contained" 
 */
@Component
public class AsyncTasks {
    
    private static StringBuffer race_condition = new StringBuffer("A Shared String");
    
    /**
     * @throws InterruptedException 
     */
    @Async
    @Scheduled(fixedRate=1000)
    public void raceConditionOne() throws InterruptedException {
        race_condition.replace(0, race_condition.length(), "One One");
        Thread.sleep(100);
        System.out.println("1 => \t" + race_condition);
    }
    
    /**
     * @throws InterruptedException 
     */
    @Async
    @Scheduled(fixedRate=1000)
    public void raceConditionTwo() throws InterruptedException {
        race_condition.replace(0, race_condition.length(), "Two Two");
        Thread.sleep(100);
        System.out.println("2 => \t" + race_condition);
    }
    
    /**
     * @throws InterruptedException 
     */
    @Async
    @Scheduled(fixedRate=1000)
    public void raceConditionThree() throws InterruptedException {
        race_condition.replace(0, race_condition.length(), "Three Three");
        Thread.sleep(100);
        System.out.println("3 => \t" + race_condition);
    }
    
    /**
     * @throws InterruptedException 
     */
    @Async
    @Scheduled(fixedRate=1000)
    public void raceConditionFour() throws InterruptedException {
        race_condition.replace(0, race_condition.length(), "Four Four");
        Thread.sleep(100);
        System.out.println("4 => \t" + race_condition);
    }
    
}
