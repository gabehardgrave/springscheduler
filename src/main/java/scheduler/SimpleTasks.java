/**
 * Tasks to be scheduled
 */
package scheduler;


import java.sql.Timestamp;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * A set of simple tasks to be scheduled.
 * Each should run concurrently
 * @author gshardgrave
 */
@Component
public class SimpleTasks {

    private long print_id = 0;
    
    /**
     * Prints to the console
     * 
     * "fixedRate" indicates that this runs every 2 seconds, starting from the
     * start time of each method call.
     */
    @Scheduled(fixedRate=2000)
    public void printIdToConsole() {
        System.out.println("Scheduled Print => " + this.print_id);
        this.print_id += 1;
    }
    
    /**
     * Returns an arbitrary message
     * 
     * "fixedDelay" indicates that this runs every 2 seconds, starting from the
     * end time of each method call.
     * 
     * "initialDelay" delays the first invocation for 4 seconds
     *  
     * @return arbitrary message
     */
    @Scheduled(initialDelay=4000, fixedDelay=1000)
    public String printMsgToConsole() {
        String arbitrary_msg = "This is an arbitrary message";
        System.out.println(arbitrary_msg);
        return arbitrary_msg;
        // Spring documentation says all Scheduled tasks should have void returns,
        // although this function seems to work fine
    }
    
    /**
     * This should only run on weekdays (more complicated cron specifications are also supported).
     * Right now, this prints out every 5 seconds
     */
    @Scheduled(cron="*/5 * * * * MON-FRI")
    public void printOnWeekdays() {
        Timestamp t = new Timestamp(System.currentTimeMillis());
        System.out.println("This is a weekday task, current => " + t);
    }
    
    // This will cause a crash. Scheduled tasks should be parameter-less, according to the
    // documentation
    /*
    @Scheduled(fixedRate=3500)
    public void printArgToConsole(String arg) {
        System.out.println(arg);
    }
    */
    
}
