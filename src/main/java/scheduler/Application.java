/**
 * Main for Spring Scheduler
 */
package scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Main driver for Spring Scheduler
 */
@Configuration           //
@EnableAutoConfiguration // These three could be set through @SpringBootApplication instead
@ComponentScan           // 
@EnableScheduling
@EnableAsync
public class Application {

    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        
        // Modify app settings 
        
        app.run(args);
    }

}
