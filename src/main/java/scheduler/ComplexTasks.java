/**
 * An example of scheduling slightly more complicated tasks
 */
package scheduler;

import java.io.IOException;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * All tasks are Asynchronous, but they should be self contained. Printing to the console might
 * be staggered, but that's fine.
 */
@Component
public class ComplexTasks {
    
    
    /**
     * 
     */
    @Async
    @Scheduled(fixedRate=5000)
    public void runFileOne() {
        String fname = "resources/file_one.py";
        System.out.println("Running " + fname);
        
        ProcessBuilder pb = new ProcessBuilder("python", fname).inheritIO();
        try {
            pb.start();
            System.out.println("Finished Running " + fname);
        } catch (IOException e) {
            System.out.println("Could not run " + fname);
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * 
     */
    @Async
    @Scheduled(fixedRate=5000)
    public void runFileTwo() {
        String fname = "resources/file_two.py";
        System.out.println("Running " + fname);
        
        ProcessBuilder pb = new ProcessBuilder("python", fname).inheritIO();
        try {
            pb.start();
            System.out.println("Finished Running " + fname);
        } catch (IOException e) {
            System.out.println("Could not run " + fname);
            System.out.println(e.getMessage());
        }
    }
}
