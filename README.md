# Task Scheduling with Spring

For more information on Scheduling with Spring, see the documentation [here](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/scheduling.html#scheduling-introduction).

This application is meant to explore and practice task Scheduling in Spring. All of the relevant code is in `src/main/java/scheduler`.

Individual logic for the different tasks are in `SimpleTasks.java`, `AsyncTasks.java`, and `ComplexTasks.java`. The main "driver" for the program is in `Application.java`.

`Application.java` is fairly bare bones (although from what I could tell, standard for a Spring application). There are two lines worth talking about.

```java
// in Application.java
@EnableScheduling       // This is what tells the methods annotated with @Scheduled() to run
@EnableAsync            // This is what tells the methods annotated with @Async to run
public class Application { . . . }
```

### SimpleTasks.java ###

There are three methods, each annotated with `@Scheduled(param=arg)`. Each one is fairly straightforward, printing a specific message to the console at the specified intervals. Each task, while independent, occurs in the same thread. This demonstrates pretty basic Scheduling.

### AsyncTasks.java ###

There are four methods, each with `@Scheduled()` and, more importantly, an `@Async` annotations. The goal of these tasks was to see how much synchronization Spring uses for `@Async` routines, by manipulating and printing shared data.

The answer seems to be None.

Each method prints out a number, followed by the number spelled out twice, like so.
`1 => One One`

If each `@Async` method was synchronized, output would look something like this.
```
# order doesn't matter
1 => One One
4 => Four Four
2 => Two Two
3 => Three Three
```
However, output looks like this instead.
```
4 => 	Two Two
1 => 	Two Two
3 => 	Two Two
2 => 	Two Two
```

Which is to say that Spring doesn't make an effort to lock shared data between `@Async` threads. This isn't really surprising, but still important to verify.

`@Async`ed tasks are still tremendously useful, they just need to be independent of one another. 


### ComplexTasks.java ###

There are only two tasks, both involving printing out a descriptive message, running external python programs, and capturing those programs' output. These tasks are probably a little bit more "realistic" than tasks from the other files. Output is staggered, which is both consistent with what you'd expect from asynchronous tasks, and also not a problem, since both are independent.

## Testing ##

In the root directory, run the following.

`./gradlew bootRun`

You should see something like this.

```
. . .
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.5.2.RELEASE)

2017-06-14 15:39:57.711  INFO 3824 --- [  restartedMain] scheduler.Application                    : Starting Application on SEC10007 with PID 3824 (C:\Users\gshardgrave\dev\springscheduler\build\classes\main started by gshardgrave in C:\Users\gshardgrave\dev\springscheduler)
. . .
```

Followed up by something like this:

```
Running resources/file_two.py
Scheduled Print => 0
This is textual data from file TWO
Running resources/file_one.py
Finished Running resources/file_two.py
Finished Running resources/file_one.py
4 =>    Three Three
2 =>    Three Three
1 =>    Three Three
3 =>    Three Three
This is textual data from file ONE
1 =>    Four Four
3 =>    Four Four
4 =>    Four Four
2 =>    Four Four
This is a weekday task, current => 2017-06-14 15:40:05.001
Scheduled Print => 1
1 =>    Four Four
3 =>    Four Four
4 =>    Four Four
2 =>    Four Four
1 =>    Four Four
3 =>    Four Four
2 =>    Four Four
4 =>    Four Four
Scheduled Print => 2
This is an arbitrary message
```